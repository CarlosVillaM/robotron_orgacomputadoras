# Robotron_OrgaComputadoras

Robotron

![Robotron Screenshot](/images/robotron_screenshot.png)

Alcance del juego:

El juego constará de 3 niveles (waves) en los que que el personaje principal trata de salvar a los humanos en cada nivel al destruir a los robots o monstruos.
El personaje gana puntos al destruir a los robots:
	*Robot tipo 1: 200 puntos
	*Robot tipo 2: 250 puntos
	*Robot tipo 3: 300 puntos.

Por cada humano que salve gana 1000 puntos.

Al principio el personaje principal cuenta con una cantidad de vida y escudo. Mientras tenga el escudo el personaje no podrá perder vida al ser atacado por
un robot. El escudo no dura para siempre, solo podrá recibir cierta cantidad de daño y luego se destruye, a partir de allí cualquier daño que el personaje
reciba afectará a la vida de este. 

Por cada wave se aumentará la dificultad del juego añadiendo más robots y un diferente tipo de robot. El Personaje gana al sobrevivir a las 3 waves.

